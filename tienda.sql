-- EJERCICIO DE BASE DE DATOS POR GUILLERMO MONTERO MARTÍN
-- MÓDULO BASES DE DATOS
-- 2019


-- CREACIÓN DE LA BBDD TIENDA
mysql> create database tienda;


-- MUESTRO LAS BB.DD
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| centroDeportivo    |
| information_schema |
| libreria           |
| mysql              |
| performance_schema |
| sys                |
| tienda             |
| world              |
+--------------------+


-- USO LA BBDD TIENDA
mysql> use tienda;


-- CREO LA TABLA PRODUCTOS
mysql> create table productos (
    -> codigo varchar(10) not null primary key auto_increment,
    -> nombre varchar(30) not null,
    -> precio float(5,2),
    -> fechadealta date,
    -> categoria varchar(50)
);


-- MUESTRO LA TABLA CREADA
mysql> show tables;
+------------------+
| Tables_in_tienda |
+------------------+
| productos        |
+------------------+


-- INSERTO 10 REGISTROS EN LA TABLA PRODUCTOS
mysql> insert into productos values
    -> ('c01','Cepillo abrillantar', 2.50, '2017-11-02'),
    -> ('r01','Regleta mod. ZAZ', 10, '2018-05-03'),
    -> ('r02','Regleta mod. XAX', 15, '2018-05-03'),
    -> ('p01','Pegamento rápido', 6.50, '2017-11-02'),
    -> ('p02','Pegamento industrial', 14.50, '2017-10-06'),
    -> ('a03','Azada grande 60cm', 50.60, '2018-05-03'),
    -> ('c02','Cepillo birutas', 6.20, '2017-10-06'),
    -> ('c03','Cepillo jardín', 22.35, '2018-05-03'),
    -> ('a01','Azada pequeña 30cm', 25, '2017-10-06'),
    -> ('a02','Azada mediana 45 cm', 37.50, '2017-11-02');


-- MUESTRO LOS REGISTROS INSERTADOS
mysql> select * from productos;
+--------+--------------------------+--------+-------------+
| codigo | nombre                   | precio | fechadealta |
+--------+--------------------------+--------+-------------+
| a01    | Azada pequeña 30cm       |  25.00 | 2017-10-06  |
| a02    | Azada mediana 45 cm      |  37.50 | 2017-11-02  |
| a03    | Azada grande 60cm        |  50.60 | 2018-05-03  |
| c01    | Cepillo abrillantar      |   2.50 | 2017-11-02  |
| c02    | Cepillo birutas          |   6.20 | 2017-10-06  |
| c03    | Cepillo jardín           |  22.35 | 2018-05-03  |
| c04    | Cepillo Birutas Pequeño  |   6.30 | 2019-04-05  |
| p01    | Pegamento rápido         |   6.50 | 2017-11-02  |
| p02    | Pegamento industrial     |  14.50 | 2017-10-06  |
| r01    | Regleta mod. ZAZ         |  10.00 | 2018-05-03  |
| r02    | Regleta mod. XAX         |  15.00 | 2018-05-03  |
+--------+--------------------------+--------+-------------+


-- MUESTRO LOS PRODUCTOS QUE TIENEN EL NOMBRE DE CEPILLO
mysql> select * from productos where nombre like "%cepillo%";
+--------+--------------------------+--------+-------------+
| codigo | nombre                   | precio | fechadealta |
+--------+--------------------------+--------+-------------+
| c01    | Cepillo abrillantar      |   2.50 | 2017-11-02  |
| c02    | Cepillo birutas          |   6.20 | 2017-10-06  |
| c03    | Cepillo jardín           |  22.35 | 2018-05-03  |
| c04    | Cepillo Birutas Pequeño  |   6.30 | 2019-04-05  |
+--------+--------------------------+--------+-------------+


-- MUESTRO LOS PRODUCTOS CUYO NOMBRE TERMINA EN 'M'
mysql> select * from productos where nombre like "%m";
+--------+---------------------+--------+-------------+
| codigo | nombre              | precio | fechadealta |
+--------+---------------------+--------+-------------+
| a01    | Azada pequeña 30cm  |  25.00 | 2017-10-06  |
| a02    | Azada mediana 45 cm |  37.50 | 2017-11-02  |
| a03    | Azada grande 60cm   |  50.60 | 2018-05-03  |
+--------+---------------------+--------+-------------+


-- MUESTRO EL NOMBRE Y PRECIO DE LOS PRODUCTOS QUE CUESTAN MÁS DE 22
mysql> select nombre, precio from productos where precio > 22;
+---------------------+--------+
| nombre              | precio |
+---------------------+--------+
| Azada pequeña 30cm  |  25.00 |
| Azada mediana 45 cm |  37.50 |
| Azada grande 60cm   |  50.60 |
| Cepillo jardín      |  22.35 |
+---------------------+--------+


--MUESTRO EL PRECIO MEDIO DE TODOS LOS PRODUCTOS QUE EMPIEZAN CON LA PALABRA 'REGLETA'
mysql> select avg(precio) from productos where nombre like "regleta%";
+-------------+
| avg(precio) |
+-------------+
|   12.500000 |
+-------------+


-- MODIFICO LA ESTRUCTURA DE LA TABLA Y AÑADO EL NUEVO CAMPO 'CATEGORÍA'
mysql> alter table productos add categroria varchar(50) not null;


-- LE DOY A TODOS LOS PRODUCTOS LA CATEGORÍA 'UTENSILIO'
mysql> update productos set categoria = "utensilio";


-- LE DOY A TODOS LOS PRODUCTOS CUYO NOMBRE CONTIENE LA PALABRA 'REGLETA' LA CATEGORÍA 'ENCHUFES'
mysql> update productos set categoria = "enchufes" where nombre like "%regleta%";


-- MUESTRO LAS DIFERENTES CATEGORÍAS DE PRODUCTOS QUE HAY EN LA BBDD
mysql> select distinct categoria from productos;
+-----------+
| categoria |
+-----------+
| utensilio |
| enchufe   |
+-----------+


-- MUESTRO LA CANTIDAD DE PRODUCTO QUE TENGO EN CADA CATEGORÍA
mysql> select distinct categoria, count(*) from productos group by categoria;
+-----------+----------+
| categoria | count(*) |
+-----------+----------+
| utensilio |        8 |
| enchufe   |        2 |
+-----------+----------+


-- RENOKMBRO LA TABLA PRODUCTOS POR 'PRODUCTOSB'
mysql> alter table productos rename ProductosB;


-- AGREGO NUEVO CAMPO 'CANTIDAD' A LA TABLA PRODUCTOSB
mysql> alter table ProductosB add cantidad int(5) not null after fechadealta;


-- MUESTRO TODOS LOS DATOS DE LOS REGISTROS QUE TENGO EN LA TABLA PRODUCTOSB
mysql> select * from ProductosB;
+--------+--------------------------+--------+-------------+----------+-----------+
| codigo | nombre                   | precio | fechadealta | cantidad | categoria |
+--------+--------------------------+--------+-------------+----------+-----------+
| a01    | Azada pequeña 30cm       |  25.00 | 2017-10-06  |        0 | utensilio |
| a02    | Azada mediana 45 cm      |  37.50 | 2017-11-02  |        0 | utensilio |
| a03    | Azada grande 60cm        |  50.60 | 2018-05-03  |        0 | utensilio |
| c01    | Cepillo abrillantar      |   2.50 | 2017-11-02  |        0 | utensilio |
| c02    | Cepillo birutas          |   6.20 | 2017-10-06  |        0 | utensilio |
| c03    | Cepillo jardín           |  22.35 | 2018-05-03  |        0 | utensilio |
| c04    | Cepillo Birutas Pequeño  |   6.30 | 2019-04-05  |        0 | NULL      |
| p01    | Pegamento rápido         |   6.50 | 2017-11-02  |        0 | utensilio |
| p02    | Pegamento industrial     |  14.50 | 2017-10-06  |        0 | utensilio |
| r01    | Regleta mod. ZAZ         |  10.00 | 2018-05-03  |        0 | enchufe   |
| r02    | Regleta mod. XAX         |  15.00 | 2018-05-03  |        0 | enchufe   |
+--------+--------------------------+--------+-------------+----------+-----------+


-- ACTUALIZO LA TABLA PRODUCTOSB ASIGNANDOLES A TODOS LOS PRODUCTOS LA CANTIDAD '100'
mysql> update ProductosB set cantidad = 100;


-- LE DOY A LOS PRODUCTOS CUYO NOMBRE CONTIENE LA PALABRA 'PEGAMENTO' LA CATEGORIA 'STICK'
mysql> update ProductosB set categoria = "stick" where nombre like "%Pegamento%";


-- MODIFICO LA CANTIDAD A 50 EN TODOS AQUELLOS PRODUCTOS CUYO PRECIO ES MENOR DE 10
mysql> update ProductosB set cantidad = 50 where precio < 10;


-- MUESTRO LAS DIFERENTES CATEGORÍAS DE PRODUCTOS
mysql> select distinct categoria from ProductosB;
+-----------+
| categoria |
+-----------+
| utensilio |
| NULL      |
| stick     |
| enchufe   |
+-----------+


-- MUESTRO LAS DIFERENTES CATEGORÍAS DE PRODUCTOS Y SU CANTIDAD
mysql> select distinct categoria, count(*) from ProductosB group by categoria;
+-----------+----------+
| categoria | count(*) |
+-----------+----------+
| utensilio |        6 |
| NULL      |        1 |
| stick     |        2 |
| enchufe   |        2 |
+-----------+----------+


-- MUESTRO LAS DIFERENTES CATEGORÍAS DE LA TABLA PRODUCTOSB ASIGNANDOLE EL NOMBRE 'CATEGORÍAS DIFERENTES' A LA QUERY
mysql> select distinct(categoria) as "Categorias diferentes" from ProductosB;
+-----------------------+
| Categorias diferentes |
+-----------------------+
| utensilio             |
| NULL                  |
| stick                 |
| enchufe               |
+-----------------------+


-- LE DOY LA CATEGORÍA 'UTENDSILIO' AL PRODUCTO 'CEPILLO BIRUTAS PEQUEÑO'
mysql> update ProductosB set categoria = "utensilio" where nombre = "Cepillo Birutas Pequeño";


-- LE DOY LA CATEGORÍA 'CEPILLO' A TODOS LOS PRODUCTOS QUE TENGAN EL NOMBRE 'CEPILLO'
mysql> update ProductosB set categoria = "cepillo" where nombre = "%Cepillo%";


-- MUESTRO LOS DATOS DE LOS REGISTROS DE LA TABLA PRODUCTOSB ACTUALIZADOS
mysql> select * from ProductosB;
+--------+--------------------------+--------+-------------+----------+-----------+
| codigo | nombre                   | precio | fechadealta | cantidad | categoria |
+--------+--------------------------+--------+-------------+----------+-----------+
| a01    | Azada pequeña 30cm       |  25.00 | 2017-10-06  |      100 | utensilio |
| a02    | Azada mediana 45 cm      |  37.50 | 2017-11-02  |      100 | utensilio |
| a03    | Azada grande 60cm        |  50.60 | 2018-05-03  |      100 | utensilio |
| c01    | Cepillo abrillantar      |   2.50 | 2017-11-02  |       50 | cepillo   |
| c02    | Cepillo birutas          |   6.20 | 2017-10-06  |       50 | cepillo   |
| c03    | Cepillo jardín           |  22.35 | 2018-05-03  |      100 | cepillo   |
| c04    | Cepillo Birutas Pequeño  |   6.30 | 2019-04-05  |       50 | cepillo   |
| p01    | Pegamento rápido         |   6.50 | 2017-11-02  |       50 | stick     |
| p02    | Pegamento industrial     |  14.50 | 2017-10-06  |      100 | stick     |
| r01    | Regleta mod. ZAZ         |  10.00 | 2018-05-03  |      100 | enchufe   |
| r02    | Regleta mod. XAX         |  15.00 | 2018-05-03  |      100 | enchufe   |
+--------+--------------------------+--------+-------------+----------+-----------+
